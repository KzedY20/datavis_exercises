import { writable } from 'svelte/store';
import { scaleLinear } from 'd3-scale';

export const height = 1000;
export const width = 900;
export const margin = { top: 10, right: 10, bottom: 10, left: 10 };
export const innerWidth = width - margin.left - margin.right;
export const innerHeight = height - margin.top - margin.bottom;
export const xScale = scaleLinear().domain([0,744]).range([0,innerWidth]);
export const yscaleTick = innerHeight/16;

export const yScale = writable();
