export const width = 500;
export const height = 600;
export const margin = { top: 10, right: 10, bottom: 10, left: 10 };
export const innerWidth = width - margin.left - margin.right;
export const innerHeight = height - margin.top - margin.bottom;
export const treeNode=11/12*innerHeight;
export const treeBase=3/12*innerHeight;
export const leafHeight=1/12*innerHeight;